const orderForm = document.getElementById('orderForm');

orderForm.addEventListener('submit', function(event) {
    event.preventDefault();

    const nama = document.getElementById('nama').value;
    const kamera = document.getElementById('kamera').value;

    const pesanan = {
        nama: nama,
        kamera: kamera
    };

    fetch('proses_order.php', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(pesanan)
    })
    .then(response => {
        if (response.ok) {
            alert('Pesanan berhasil dikirim!');
            window.location.href = 'order.html';
        } else {
            alert('Terjadi kesalahan saat mengirim pesanan.');
        }
    })
    .catch(error => {
        console.error('Terjadi error:', error);
    });
});
