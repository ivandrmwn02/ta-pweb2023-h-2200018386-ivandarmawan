<?php
    $pesanan = file_get_contents("pesanan.txt");
?>

<!DOCTYPE html>
<html>
<head>
    <title>Pesanan - Re-Lens</title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <header>
        <h1>Re-Lens</h1>
    </header>
    
    <nav>
        <ul>
            <li><a href="index.html">Beranda</a></li>
            <li><a href="kamera.html">Kamera</a></li>
            <li><a href="order.html">Order</a></li>
            <li><a href="pesanan.php">Pesanan</a></li>
        </ul>
    </nav>
    
    <section>
        <h2>Data Pesanan</h2>
        <?php if (!empty($pesanan)) : ?>
            <pre><?php echo $pesanan; ?></pre>
        <?php else : ?>
            <p>Tidak ada pesanan.</p>
        <?php endif; ?>

        <?php if (isset($_GET['durasi']) && isset($_GET['harga'])) : ?>
            <h3>Detail Order:</h3>
            <p>Durasi: <?php echo $_GET['durasi']; ?> Hari</p>
            <p>Harga: <?php echo $_GET['harga']; ?></p>
        <?php endif; ?>
    </section>
    
    <footer>
        <p>Hak Cipta &copy; 2023 Re-Lens. All rights reserved.</p>
    </footer>
</body>
</html>
